<?php

namespace AppBundle\Repository;

use Gedmo\Translatable\Entity\Repository\TranslationRepository;

class ArticleRepository extends TranslationRepository
{
    /**
     * @param string $tag
     * @param string $locale
     */
    public function findByTagAndLocale($tag, $locale)
    {
        $dql = '
            SELECT a
            FROM AppBundle\Entity\Article a
            INNER JOIN a.tags t
            WHERE t.name = :tag
        ';

        $article = $this->_em
            ->createQuery($dql)
            ->setParameter('tag', $tag)
            ->setMaxResults(1)
            ->getOneOrNullResult()
        ;

        if (null === $article) {
            return $article;
        }

        $article->setLocale($locale);
        $this->_em->refresh($article);

        return $article;
    }
}