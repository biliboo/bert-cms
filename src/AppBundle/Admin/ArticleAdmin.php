<?php

namespace AppBundle\Admin;

use AppBundle\Manager\ArticleManager;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\CallbackTransformer;

class ArticleAdmin extends AbstractAdmin
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ArticleManager
     */
    private $manager;

    /**
     * @param EntityManagerInterface $em
     */
    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ArticleManager $manager
     */
    public function setArticleManager(ArticleManager $manager)
    {
        $this->manager = $manager;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('description')
            ->add('title')
            ->add('content', TextareaType::class, [
                'required'             => false,
                'attr'                 => [
                    'data-role' => 'summernote',
                ],
            ])
            ->add('template', TextType::class, [
                'required' => false
            ])
            ->add('parameters', TextareaType::class, [
                'required' => false
            ])
            ->add('tags', 'sonata_type_collection', array(
                'by_reference' => true,
                'type_options' => array(
                    // Prevents the "Delete" option from being displayed
                    'delete' => true,
                )
            ), array(
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ))
        ;

        // We optimize display and storage space
        $formMapper->get('parameters')
            ->addModelTransformer(new CallbackTransformer(
                function ($json)
                {
                    if (null !== $json) {
                        $json = json_encode($json, JSON_PRETTY_PRINT);
                    }

                    return $json;
                },
                function ($string)
                {
                    if (null !== $string) {
                        $string = json_decode($string, true);
                    }

                    return $string;
                }
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('tags')
            ->add('description')
            ->add('title')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('description')
        ;
    }

    private function handleTags($object)
    {
        $this->em->persist($object);

        foreach($object->getTags() as $tag) {
            $tag->setArticle($object);
            $this->em->persist($tag);
        }
    }

    /**
     * @param Article $object
     */
    public function prePersist($object)
    {
        $this->handleTags($object);
    }

    /**
     * @param Article $object
     */
    public function preUpdate($object)
    {
        $this->handleTags($object);
    }

    public function postPersist($object)
    {
        $this->manager->updateArticle($object);
    }

    /**
     * @param Article $object
     */
    public function postUpdate($object)
    {
        $this->manager->updateArticle($object);
    }
}