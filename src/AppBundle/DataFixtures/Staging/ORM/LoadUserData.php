<?php

namespace AppBundle\DataFixtures\Staging\ORM;

use AppBundle\DataFixtures\UserFixture;

class LoadUserData extends UserFixture
{
    public function getData()
    {
        return [
            'admin' => [
                'uuid'  => 1,
                'roles' => ['ROLE_SUPER_ADMIN'],
            ],
        ];
    }
}