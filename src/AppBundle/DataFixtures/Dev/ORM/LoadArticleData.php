<?php

namespace AppBundle\DataFixtures\Dev\ORM;

use AppBundle\DataFixtures\ArticleFixture;

class LoadArticleData extends ArticleFixture
{
    protected function getData()
    {
        return [
            'login' => [
                'title'  => 'Login',
                'description' => "Texte sur la page de connexion.",
                'content' => "Bienvenue",
                'tags' => ['login'],
            ],
            'dashboard' => [
                'title'  => 'Tableau de bord',
                'description' => "Message en haut du tableau de bord.",
                'content' => "<p class='alert alert-info dark'>L'application CMS a bien été réinitalisée a " . date(DATE_RFC3339) . '.</p>',
                'tags' => ['dashboard'],
            ],
        ];
    }
}