<?php

namespace AppBundle\DataFixtures\Dev\ORM;

use AppBundle\DataFixtures\UserFixture;

class LoadUserData extends UserFixture
{
    protected function getData()
    {
        return [
            'admin' => [
                'uuid'  => 1,
                'roles' => ['ROLE_SUPER_ADMIN'],
            ],
        ];
    }
}