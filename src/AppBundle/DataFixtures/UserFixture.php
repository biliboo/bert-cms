<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

abstract class UserFixture extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $om)
    {
        $this->loadData($om, $data = $this->getData());

        // We simulate the subscribed event to populate the user table
        $handler = $this->container->get('bert_isp_client.event_broker_handler.user.default');

        foreach($data as $options) {
            $handler->handleSubscribedEvent($options['uuid']);
        }
    }

    abstract protected function getData();

    private function loadData(ObjectManager $om, array $data)
    {
        foreach($data as $options) {
            $user = new User();

            $user
                ->setUuid($options['uuid'])
                ->setRoles($options['roles'])
            ;

            $om->persist($user);
        }

        $om->flush();
    }

    public function getOrder()
    {
        return 100;
    }
}