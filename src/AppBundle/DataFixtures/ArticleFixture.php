<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Article;
use AppBundle\Entity\Tag;
use AppBundle\Manager\ArticleManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

abstract class ArticleFixture extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $om)
    {
        $this->loadData($om, $this->getData());

        $this->loadData($om, $this->loadAgencies());
    }

    abstract protected function getData();

    private function loadData(ObjectManager $om, array $data)
    {
        foreach($data as $options) {
            $article = new Article();

            $article
                ->setTitle($options['title'])
                ->setDescription($options['description'])
                ->setContent($options['content'])
                ->setParameters(array_key_exists('parameters', $options) ? $options['parameters'] : null)
                ->setTemplate(array_key_exists('template', $options) ? $options['template'] : null)
                ->setOptions(array_key_exists('options', $options) ? $options['options'] : null)
                ->setLocale('fr')
            ;

            // TODO handle translations...

            $om->persist($article);

            foreach($options['tags'] as $name) {
                // TODO, handle same tags?
                $tag = new Tag();

                $tag
                    ->setName($name)
                ;

                $article->addTag($tag);

                $om->persist($tag);
            }

            $om->flush();

            $this->getArticleManager()->updateArticle($article);
        }
    }

    /**
     * @return ArticleManager
     */
    private function getArticleManager()
    {
        return $this->container->get(ArticleManager::class);
    }

    public function getOrder()
    {
        return 100;
    }


    private function loadAgencies()
    {
        $data = [
            1 => [
                'SOCCODE' => 'SFB26STRA',
                'SOCNOM' => 'SOCIETE FINANCIERE BERT',
                'SOCADR1' => 'ZI LA TULANDIERE',
                'SOCADR2' => 'BP 61',
                'PAYISO' => 'FR',
                'SOCCP' => 26140,
                'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                'SOCTEL' => '+33 (0)4 75 31 01 15',
                'SOCFAX' => '+33 (0)4 75 31 14 89',
                'SOCEMAIL' => 'bert@bert.fr',
            ], 2 => [
                'SOCCODE' => 'BERL26STR',
                'SOCNOM' => 'BERT LOGISTIQUE',
                'SOCADR1' => 'ZI LA TULANDIERE',
                'SOCADR2' => 'BP 61',
                'PAYISO' => 'FR',
                'SOCCP' => 26140,
                'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                'SOCTEL' => '+ 33 (0)4 75 31 01 15',
                'SOCFAX' => '+ 33 (0)4 75 31 14 89',
                'SOCEMAIL' => 'bert@bert.fr',
            ], 3 => [
                'SOCCODE' => 'BERT26STR',
                'SOCNOM' => 'BERT TRANSPORTS ET SERVICES',
                'SOCADR1' => 'ZI LA TULANDIERE',
                'SOCADR2' => 'BP 61',
                'PAYISO' => 'FR',
                'SOCCP' => 26140,
                'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                'SOCTEL' => '+33 (0)4 75 31 01 15',
                'SOCFAX' => '+33 (0)4 75 31 14 82',
                'SOCEMAIL' => 'bert@bert.fr',
            ],
            7 =>
                [
                    'SOCCODE' => 'BSS26STRA',
                    'SOCNOM' => 'BERT STOCKAGE & SERVICES',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => 'BP61',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 89',
                    'SOCEMAIL' => '',
                ],
            8 =>
                [
                    'SOCCODE' => 'BOUV26STR',
                    'SOCNOM' => 'BERT TRUCKS',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => 'BP 61',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 89',
                    'SOCEMAIL' => 'bert@bert.fr',
                ],
            9 =>
                [
                    'SOCCODE' => 'BDFT42SOR',
                    'SOCNOM' => 'BERT DU FOREZ - BDF',
                    'SOCADR1' => 'RUE JEAN BERTHON',
                    'SOCADR2' => 'ZI DE LA VAURE',
                    'PAYISO' => 'FR',
                    'SOCCP' => 42290,
                    'SOCVILLIB1' => 'SORBIERS',
                    'SOCTEL' => '+33 (0)4 77 01 01 36',
                    'SOCFAX' => '+33 (0)4 77 53 43 05',
                    'SOCEMAIL' => '',
                ],
            10 =>
                [
                    'SOCCODE' => 'BENE26STR',
                    'SOCNOM' => 'BERT ENERGIE',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)1 60 96 39 88',
                    'SOCFAX' => '+33 (0)1 64 23 18 35',
                    'SOCEMAIL' => '',
                ],
            12 =>
                [
                    'SOCCODE' => 'BERT30MAN',
                    'SOCNOM' => 'BERT NIMES',
                    'SOCADR1' => 'ROUTE DE BEAUCAIRE',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 30129,
                    'SOCVILLIB1' => 'MANDUEL',
                    'SOCTEL' => '+33 (0)4 66 20 84 40',
                    'SOCFAX' => '+33 (0)4 66 20 84 41',
                    'SOCEMAIL' => '',
                ],
            13 =>
                [
                    'SOCCODE' => 'BERT42SUR',
                    'SOCNOM' => 'BERT 42',
                    'SOCADR1' => 'RUE DE LA FETE DIEU',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 42450,
                    'SOCVILLIB1' => 'SURY LE COMTAL',
                    'SOCTEL' => '+33 (0)4 77 50 56 70',
                    'SOCFAX' => '+33 (0)4 77 50 56 71',
                    'SOCEMAIL' => '',
                ],
            14 =>
                [
                    'SOCCODE' => 'BERT45COU',
                    'SOCNOM' => 'BERT 45',
                    'SOCADR1' => 'LES ALLIOTS',
                    'SOCADR2' => 'BP 2',
                    'PAYISO' => 'FR',
                    'SOCCP' => 45720,
                    'SOCVILLIB1' => 'COULLONS',
                    'SOCTEL' => '+33 (0)2 38 36 11 51',
                    'SOCFAX' => '+33 (0)2 38 36 10 58',
                    'SOCEMAIL' => '',
                ],
            15 =>
                [
                    'SOCCODE' => 'BERT84SOR',
                    'SOCNOM' => 'BERT PROVENCE',
                    'SOCADR1' => 'AVENUE DES FRERES LUMIERE',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 84700,
                    'SOCVILLIB1' => 'SORGUES',
                    'SOCTEL' => '+33 (0)4 90 01 22 25',
                    'SOCFAX' => '+33 (0)4 90 32 65 76',
                    'SOCEMAIL' => '',
                ],
            16 =>
                [
                    'SOCCODE' => 'BESS69STP',
                    'SOCNOM' => 'BESSENAY',
                    'SOCADR1' => '7 RUE DU LYONNAIS',
                    'SOCADR2' => 'ZONE INDUSTRIELLE',
                    'PAYISO' => 'FR',
                    'SOCCP' => 69800,
                    'SOCVILLIB1' => 'SAINT PRIEST',
                    'SOCTEL' => '+33 (0)4 78 21 33 44',
                    'SOCFAX' => '+33 (0)4 78 21 33 99',
                    'SOCEMAIL' => '',
                ],
            17 =>
                [
                    'SOCCODE' => 'BOUS26STD',
                    'SOCNOM' => 'BOUVAREL STOCKAGE',
                    'SOCADR1' => '',
                    'SOCADR2' => 'ZA - 41 RUE DES SAULES',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26260,
                    'SOCVILLIB1' => 'SAINT DONAT SUR L\'HERBASSE',
                    'SOCTEL' => '+33 (0)4 75 45 17 02',
                    'SOCFAX' => '+33 (0)4 75 45 08 04',
                    'SOCEMAIL' => '',
                ],
            18 =>
                [
                    'SOCCODE' => 'BRA69MEYZ',
                    'SOCNOM' => 'BERT RHONE ALPES',
                    'SOCADR1' => '20 RUE JEAN JAURES',
                    'SOCADR2' => 'CS 30160',
                    'PAYISO' => 'FR',
                    'SOCCP' => 69330,
                    'SOCVILLIB1' => 'MEYZIEU CEDEX',
                    'SOCTEL' => '+33 (0)4 78 80 34 33',
                    'SOCFAX' => '+33 (0)4 78 31 66 42',
                    'SOCEMAIL' => '',
                ],
            19 =>
                [
                    'SOCCODE' => 'GBTL26STR',
                    'SOCNOM' => 'GB TRANSPORT & LOGISTIQUE',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => 'BP61',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 89',
                    'SOCEMAIL' => '',
                ],
            20 =>
                [
                    'SOCCODE' => 'TGB77NEMO',
                    'SOCNOM' => 'T.G.B. GRAILLOT - BERT',
                    'SOCADR1' => '8, RUE DES MOINES',
                    'SOCADR2' => 'ZONE INDUSTRIELLE',
                    'PAYISO' => 'FR',
                    'SOCCP' => 77140,
                    'SOCVILLIB1' => 'NEMOURS',
                    'SOCTEL' => '+33 (0)1 60 55 21 98',
                    'SOCFAX' => '+33 (0)1 60 55 13 50',
                    'SOCEMAIL' => '',
                ],
            21 =>
                [
                    'SOCCODE' => 'BERS26STR',
                    'SOCNOM' => 'BERT SERVICES',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => 'BP 61',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 88 24',
                    'SOCFAX' => '+33 (0)4 75 31 16 41',
                    'SOCEMAIL' => '',
                ],
            22 =>
                [
                    'SOCCODE' => 'BVS07ANNO',
                    'SOCNOM' => 'BERT VIVARAIS STOCKAGE',
                    'SOCADR1' => '6 RUE DES SOURCES',
                    'SOCADR2' => 'PARC D\'ACTIVITE DE MARENTON',
                    'PAYISO' => 'FR',
                    'SOCCP' => 7100,
                    'SOCVILLIB1' => 'ANNONAY',
                    'SOCTEL' => '+33 (0)4 75 33 02 38',
                    'SOCFAX' => '+33 (0)4 75 33 02 39',
                    'SOCEMAIL' => '',
                ],
            23 =>
                [
                    'SOCCODE' => 'POIR45COU',
                    'SOCNOM' => 'POIRIER',
                    'SOCADR1' => 'LES ALLIOTS',
                    'SOCADR2' => 'BP 2',
                    'PAYISO' => 'FR',
                    'SOCCP' => 45720,
                    'SOCVILLIB1' => 'COULLONS',
                    'SOCTEL' => '+33 (0)2 38 36 11 51',
                    'SOCFAX' => '+33 (0)2 38 36 10 58',
                    'SOCEMAIL' => '',
                ],
            24 =>
                [
                    'SOCCODE' => 'TRB37SORI',
                    'SOCNOM' => 'TRB TRANSPORTS',
                    'SOCADR1' => 'LES FOURNEAUX',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 37250,
                    'SOCVILLIB1' => 'SORIGNY',
                    'SOCTEL' => '+33 (0)2 47 80 02 11',
                    'SOCFAX' => '+33 (0)2 47 80 01 46',
                    'SOCEMAIL' => '',
                ],
            25 =>
                [
                    'SOCCODE' => 'DROM26STR',
                    'SOCNOM' => 'DROME LOGISTIQUE',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => 'BP 61',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 82',
                    'SOCEMAIL' => '',
                ],
            26 =>
                [
                    'SOCCODE' => 'INDR37SOR',
                    'SOCNOM' => 'INDRE LOGISTIQUE',
                    'SOCADR1' => '4 PLACE HENRI GUILLAUMET',
                    'SOCADR2' => 'ZONE ISOPARC',
                    'PAYISO' => 'FR',
                    'SOCCP' => 37250,
                    'SOCVILLIB1' => 'SORIGNY',
                    'SOCTEL' => '+33 (0)2 47 80 01 73',
                    'SOCFAX' => '+33 (0)2 47 40 05 02',
                    'SOCEMAIL' => '',
                ],
            28 =>
                [
                    'SOCCODE' => 'INTE26STR',
                    'SOCNOM' => 'INTERFIDUCIAIRE',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 89',
                    'SOCEMAIL' => '',
                ],
            29 =>
                [
                    'SOCCODE' => 'BOST26STR',
                    'SOCNOM' => 'TRANSPORTS BOSTYN',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 89',
                    'SOCEMAIL' => '',
                ],
            30 =>
                [
                    'SOCCODE' => 'BENE38STQ',
                    'SOCNOM' => 'BERT ENERGIE SAINT QUENTIN',
                    'SOCADR1' => 'CHEZ AIR PRODUCTS',
                    'SOCADR2' => '95 AVENUE ARRIVAUX',
                    'PAYISO' => 'FR',
                    'SOCCP' => 38070,
                    'SOCVILLIB1' => 'SAINT QUENTIN FALLAVIER',
                    'SOCTEL' => '+33 (0)6 72 63 83 43',
                    'SOCFAX' => '',
                    'SOCEMAIL' => '',
                ],
            31 =>
                [
                    'SOCCODE' => 'BERN26STR',
                    'SOCNOM' => 'BERT NORD',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 89',
                    'SOCEMAIL' => 'bert@bert.fr',
                ],
            32 =>
                [
                    'SOCCODE' => 'BENE78MAU',
                    'SOCNOM' => 'BERT ENERGIE MAUREPAS',
                    'SOCADR1' => 'CHEZ AIR PRODUCTS',
                    'SOCADR2' => 'RUE MARIE CURIE',
                    'PAYISO' => 'FR',
                    'SOCCP' => 78310,
                    'SOCVILLIB1' => 'MAUREPAS',
                    'SOCTEL' => '+33 (0)1 30 66 46 91',
                    'SOCFAX' => '',
                    'SOCEMAIL' => '',
                ],
            34 =>
                [
                    'SOCCODE' => 'BENE67SCH',
                    'SOCNOM' => 'BERT ENERGIE SCHILTIGHEIM',
                    'SOCADR1' => 'CHEZ AIR PRODUCTS',
                    'SOCADR2' => '4 AVENUE PIERRE MENDES FRANCE',
                    'PAYISO' => 'FR',
                    'SOCCP' => 67300,
                    'SOCVILLIB1' => 'SCHILTIGHEIM',
                    'SOCTEL' => '+33 (0)6 72 63 83 43',
                    'SOCFAX' => '',
                    'SOCEMAIL' => '',
                ],
            35 =>
                [
                    'SOCCODE' => 'BENE13ROG',
                    'SOCNOM' => 'BERT ENERGIE ROGNAC',
                    'SOCADR1' => 'CHEZ AIR PRODUCTS',
                    'SOCADR2' => 'ZI NORD - RUE GUSTAVE EIFFEL',
                    'PAYISO' => 'FR',
                    'SOCCP' => 13340,
                    'SOCVILLIB1' => 'ROGNAC',
                    'SOCTEL' => '+33 (0)6 72 63 83 43',
                    'SOCFAX' => '',
                    'SOCEMAIL' => '',
                ],
            36 =>
                [
                    'SOCCODE' => 'BERN62FEU',
                    'SOCNOM' => 'BERT NORD FEUCHY',
                    'SOCADR1' => '120 ALLEE DU DANEMARK',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 62223,
                    'SOCVILLIB1' => 'FEUCHY',
                    'SOCTEL' => '+33 (0)3 21 55 14 88',
                    'SOCFAX' => '+33 (0)3 21 55 06 52',
                    'SOCEMAIL' => 'bert@bert.fr',
                ],
            37 =>
                [
                    'SOCCODE' => 'BERN76ETA',
                    'SOCNOM' => 'BERT NORD ETAINHUS',
                    'SOCADR1' => '355 ROUTE DE LA LINERIE',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 76430,
                    'SOCVILLIB1' => 'ETAINHUS',
                    'SOCTEL' => '+33 (0)2 35 24 50 15',
                    'SOCFAX' => '+33 (0)2 35 26 54 47',
                    'SOCEMAIL' => 'bert@bert.fr',
                ],
            38 =>
                [
                    'SOCCODE' => 'BDEV26STR',
                    'SOCNOM' => 'BERT DEVELOPPEMENT',
                    'SOCADR1' => 'ZI LA TULANDIERE - BP61',
                    'SOCADR2' => '2 ROUTE DE LA MAISON BLANCHE',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 82',
                    'SOCEMAIL' => 'bert@bert.fr',
                ],
            39 =>
                [
                    'SOCCODE' => 'BERT13VIT',
                    'SOCNOM' => 'BERT SUD',
                    'SOCADR1' => '14-18 RUE DE BERLIN',
                    'SOCADR2' => 'ZI L\'ANJOLY',
                    'PAYISO' => 'FR',
                    'SOCCP' => 13127,
                    'SOCVILLIB1' => 'VITROLLES',
                    'SOCTEL' => '+33 (0)4 42 39 42 83',
                    'SOCFAX' => '+33 (0)4 42 45 14 50',
                    'SOCEMAIL' => '',
                ],
            40 =>
                [
                    'SOCCODE' => 'BIND26STR',
                    'SOCNOM' => 'BERT INDUSTRIE',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => '2 ROUTE DE LA MAISON BLANCHE',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 89',
                    'SOCEMAIL' => '',
                ],
            42 =>
                [
                    'SOCCODE' => 'BIND78GAR',
                    'SOCNOM' => 'BERT INDUSTRIE GARGENVILLE',
                    'SOCADR1' => '20 RUE DES GARENNES',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 78440,
                    'SOCVILLIB1' => 'GARGENVILLE',
                    'SOCTEL' => '',
                    'SOCFAX' => '',
                    'SOCEMAIL' => '',
                ],
            43 =>
                [
                    'SOCCODE' => 'BSA26STR',
                    'SOCNOM' => 'BSA',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => 'BP 61',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 82',
                    'SOCEMAIL' => 'bert@bert.fr',
                ],
            44 =>
                [
                    'SOCCODE' => 'TESTBERT',
                    'SOCNOM' => 'BERT SOCIETE TEST',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => 'BP 61',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+33 (0)4 75 31 14 82',
                    'SOCEMAIL' => 'bert@bert.fr',
                ],
            45 =>
                [
                    'SOCCODE' => 'FBI26STR',
                    'SOCNOM' => 'FONCIERE BERT INVEST',
                    'SOCADR1' => 'ZI LA TULANDIERE',
                    'SOCADR2' => '2 ROUTE DE LA MAISON BLANCHE',
                    'PAYISO' => 'FR',
                    'SOCCP' => 26140,
                    'SOCVILLIB1' => 'SAINT RAMBERT D\'ALBON',
                    'SOCTEL' => '+33 (0)4 75 31 01 15',
                    'SOCFAX' => '+ 33 (0)4 75 31 14 89',
                    'SOCEMAIL' => '',
                ],
            46 =>
                [
                    'SOCCODE' => 'EASY37SOR',
                    'SOCNOM' => 'EASY-LOG',
                    'SOCADR1' => 'ZA LES FOURNEAUX',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 37250,
                    'SOCVILLIB1' => 'SORIGNY',
                    'SOCTEL' => '+33 (0)2 47 80 02 11',
                    'SOCFAX' => '+33 (0)2 47 80 01 46',
                    'SOCEMAIL' => '',
                ],
            48 =>
                [
                    'SOCCODE' => 'PRES77STT',
                    'SOCNOM' => 'PRESTILOG',
                    'SOCADR1' => '10 Avenue de Gutenberg',
                    'SOCADR2' => '8 Boulevard de Strasbourg',
                    'PAYISO' => 'FR',
                    'SOCCP' => 77600,
                    'SOCVILLIB1' => 'BUSSY SAINT GEORGES',
                    'SOCTEL' => '+33 (0)1 85 90 01 20',
                    'SOCFAX' => '',
                    'SOCEMAIL' => 'prestilog@bert.fr',
                ],
            49 =>
                [
                    'SOCCODE' => 'BENE31TOU',
                    'SOCNOM' => 'BERT ENERGIE TOULOUSE',
                    'SOCADR1' => '9 Route Seysses',
                    'SOCADR2' => '',
                    'PAYISO' => 'FR',
                    'SOCCP' => 31100,
                    'SOCVILLIB1' => 'TOULOUSE',
                    'SOCTEL' => '+33 (0)5 61 07 56 16',
                    'SOCFAX' => '',
                    'SOCEMAIL' => '',
                ],
            50 =>
                [
                    'SOCCODE' => 'BIND33STL',
                    'SOCNOM' => 'BERT INDUSTRIE SAINT LOUBES',
                    'SOCADR1' => 'CHEZ PAD',
                    'SOCADR2' => '14 RUE DES BRUYERES',
                    'PAYISO' => 'FR',
                    'SOCCP' => 33450,
                    'SOCVILLIB1' => 'SAINT LOUBES',
                    'SOCTEL' => '',
                    'SOCFAX' => '',
                    'SOCEMAIL' => '',
                ],
            51 =>
                [
                    'SOCCODE' => 'BIND77MON',
                    'SOCNOM' => 'BERT INDUSTRIE MONTEREAU',
                    'SOCADR1' => 'CHEZ LINDE GAS',
                    'SOCADR2' => 'RUE DE LA BROSSE BOUTILLIER',
                    'PAYISO' => 'FR',
                    'SOCCP' => 77130,
                    'SOCVILLIB1' => 'MONTEREAU FAUT YONNE',
                    'SOCTEL' => '+33 (0)6 07 68 41 11',
                    'SOCFAX' => '',
                    'SOCEMAIL' => '',
                ],
        ];

        $articles = [];

        foreach($data as $item) {
            $articles[$item['SOCCODE']] = [
                'title'       => $item['SOCNOM'],
                'description' => $item['SOCNOM'],
                'content'     => '',
                'parameters'  => $item,
                'template'    => 'article/agency.html.twig',
                'options'     => [
                    'modal_body_classes'   => ' p-0',
                    'modal_disable_header' => true,
                ],
                'tags'        => ['andsoft:' . $item['SOCCODE']],
            ];
        }

        return $articles;
    }
}