<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\Request;

class MenuBuilder
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $checker;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @param AuthorizationCheckerInterface $checker
     * @param FactoryInterface $factory
     */
    public function __construct(
        AuthorizationCheckerInterface $checker,
        FactoryInterface $factory)
    {
        $this->checker         = $checker;
        $this->factory         = $factory;
    }

    /**
     * @param RequestStack $requestStack
     * @return FactoryInterface
     */
    public function createMainMenu(RequestStack $requestStack)
    {
        $request = $requestStack->getMasterRequest();

        $menu = $this->factory->createItem('menu.home', [
            'route'  => 'homepage',
        ]);

        $menu->addChild('menu.main', [
            'extras' => [
                'category' => true,
            ]
        ]);

        $menu->addChild('menu.dasboard', [
            'route'  => 'homepage',
            'extras' => [
                'icon' => 'fa fa-tachometer',
            ],
        ]);

        if ($this->checker->isGranted('ROLE_ADMIN')) {
            $this->createAdminMenu($request, $menu);
        }

        return $menu;
    }

    /**
     * @param ItemInterface $menu
     */
    private function createAdminMenu(Request $request, ItemInterface $menu)
    {
        $menu->addChild('menu.admin', [
            'extras' => [
                'category' => true,
            ]
        ]);

        if ($this->checker->isGranted('ROLE_SONATA_ADMIN')) {
            $menu->addChild('menu.technical_admin', [
                'route'  => 'sonata_admin_dashboard',
                'extras' => [
                    'icon' => 'fa fa-rocket',
                ]
            ]);
        }
    }
}