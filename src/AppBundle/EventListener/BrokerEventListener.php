<?php

namespace AppBundle\EventListener;

use AppBundle\Manager\HitManager;
use Bert\CmsApiBundle\Event\Broker\ArticleHitEvent;
use Bert\CmsApiBundle\Event\Broker\ArticleMissEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BrokerEventListener implements EventSubscriberInterface
{
    /**
     * @var HitManager
     */
    private $hitManager;

    /**
     * @param HitManager $hitManager
     */
    public function __construct(HitManager $hitManager)
    {
        $this->hitManager = $hitManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            ArticleHitEvent::EVENT  => 'onArticleHit',
            ArticleMissEvent::EVENT => 'onArticleMiss',
        ];
    }

    /**
     * @param ArticleHitEvent $event
     */
    public function onArticleHit(ArticleHitEvent $event)
    {
        $this->hitManager->hit(
            $event->getData('id'),
            $event->getData('tag'),
            $event->getData('uuid'),
            $event->getData('locale'),
            $event->getData('date')
        );
    }

    /**
     * @param ArticleMissEvent $event
     */
    public function onArticleMiss(ArticleMissEvent $event)
    {
        $this->hitManager->miss(
            $event->getData('tag'),
            $event->getData('uuid'),
            $event->getData('locale'),
            $event->getData('date')
        );
    }
}
