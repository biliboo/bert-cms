<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Article;
use Bert\CmsApiBundle\Event\Broker\ArticleUpdatedEvent;
use Bert\IspCoreBundle\Service\EventBroker;
use Doctrine\ORM\EntityManagerInterface;

class ArticleManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventBroker
     */
    private $broker;

    /**
     * @param EntityManagerInterface $em
     * @param EventBroker $broker
     */
    public function __construct(EntityManagerInterface $em, EventBroker $broker)
    {
        $this->em     = $em;
        $this->broker = $broker;
    }

    /**
     * @param string $tag
     * @param string $locale
     * @return Article
     */
    public function getArticleByTag($tag, $locale)
    {
        return $this->getRepository()->findByTagAndLocale(strtolower($tag), strtolower($locale));
    }

    /**
     * @param Article $article
     */
    public function updateArticle(Article $article)
    {
        $this->em->persist($article);
        $this->em->flush();

        $this->notifyArticleChanged($article);
    }

    /**
     * @param Article $article
     */
    private function notifyArticleChanged(Article $article)
    {
        $event = new ArticleUpdatedEvent();
        $tags  = [];

        foreach($article->getTags() as $tag) {
            $tags[] = $tag->getName();
        }

        if (0 === count($tags)) {
            return;
        }

        $event->setData([
            'tags'   => $tags,
            'locale' => $article->getLocale(),
        ]);

        $this->broker->publish($event, true);
    }

    /**
     * @return \AppBundle\Repository\ArticleRepository
     */
    private function getRepository()
    {
        return $this->em->getRepository(Article::class);
    }
}