<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Article;
use AppBundle\Entity\User;
use AppBundle\Entity\Hit;
use Doctrine\ORM\EntityManagerInterface;

class HitManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $tag
     * @param string $uuid
     * @param string $locale
     * @param mixed $date
     */
    public function miss($tag, $uuid, $locale, $date)
    {
        $hit = new Hit();

        if (is_numeric($date)) {
            $date = \DateTime::createFromFormat('U', $date);
        }

        $hit
            ->setUuid($uuid)
            ->setLocale($locale)
            ->setTag($tag)
            ->setDate($date)
            ->setUser($this->getUserRepository()->findOneBy(['uuid' => $uuid]))
        ;

        $this->em->persist($hit);
        $this->em->flush();
    }

    /**
     * @param Article $article
     * @param string $tag
     * @param string $uuid
     * @param string $locale
     * @param mixed $date
     */
    public function hit($article, $tag, $uuid, $locale, $date)
    {
        if (is_numeric($article)) {
            $article = $this->em->getRepository(Article::class)->find($article);
        }

        if (is_numeric($date)) {
            $date = \DateTime::createFromFormat('U', $date);
        }

        $hit = new Hit();

        $hit
            ->setArticle($article)
            ->setUuid($uuid)
            ->setLocale($locale)
            ->setTag($tag)
            ->setDate($date)
            ->setUser($this->getUserRepository()->findOneBy(['uuid' => $uuid]))
        ;

        $this->em->persist($hit);
        $this->em->flush();
    }

    /**
     * @return \AppBundle\Repository\HitRepository
     */
    private function getRepository()
    {
        return $this->em->getRepository(Hit::class);
    }

    /**
     * @return \AppBundle\Repository\UserRepository
     */
    private function getUserRepository()
    {
        return $this->em->getRepository(User::class);
    }
}