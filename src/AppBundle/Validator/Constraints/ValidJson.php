<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Validator\Constraints\ValidJsonValidator;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidJson extends Constraint
{
    public $message = 'error.invalid_json';

    public function validatedBy()
    {
        return ValidJsonValidator::class;
    }
}
