<?php

namespace AppBundle\Entity;

// use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="app_article_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={"article_id","locale","field"})}
 * )
 */
class ArticleTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=8, nullable=false)
     */
    protected $locale;

    /**
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    protected $field;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Article", inversedBy="translations")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", nullable=false)
     */
    protected $object;
}
