<?php

namespace AppBundle\Controller\Api;

use AppBundle\Manager\ArticleManager;
use AppBundle\Manager\HitManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/article")
 */
class ArticleController extends FOSRestController
{
    /**
     * @Route("/{raw}", name="api_article_show")
     * @Method("GET")
     */
    public function getAction(Request $request, $raw)
    {
        $locale = $request->query->get('l', $this->getParameter('locale'));
        $tag    = base64_decode($raw);

        $article = $this->getArticleManager()->getArticleByTag($tag, $locale);

        if (null === $article) {
            throw $this->createNotFoundException(sprintf('Article "%s" not found.', $tag));
        }

        if (null !== $article->getTemplate()) {
            $content = $this->renderView($article->getTemplate(), [
                'article' => $article,
            ]);
        } else {
            $content = $article->getContent();
        }

        return new JsonResponse([
            'id'      => $article->getId(),
            'locale'  => $article->getLocale(),
            'title'   => $article->getTitle(),
            'options' => $article->getOptions(),
            'content' => $content,
            'author'  => null !== $article->getAuthor() ? $article->getAuthor()->getUuid() : null,
        ]);
    }

    /**
     * @return ArticleManager
     */
    private function getArticleManager()
    {
        return $this->get(ArticleManager::class);
    }
}
