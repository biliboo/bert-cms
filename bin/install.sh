#!/bin/bash

APP=cms
APP_DIRECTORY=/var/www-apps/dev/$APP

sudo mkdir -p /home/vagrant/cache/$APP/logs && \
sudo chown vagrant:vagrant /home/vagrant/cache/$APP/logs && \
touch /home/vagrant/cache/$APP/logs/.gitkeep && \
sudo mkdir -p /home/vagrant/cache/$APP/cache && \
sudo chown vagrant:vagrant /home/vagrant/cache/$APP/cache && \
touch /home/vagrant/cache/$APP/cache/.gitkeep && \
rm -rf "$APP_DIRECTORY/var/logs" && \
ln -s /home/vagrant/cache/$APP/logs "$APP_DIRECTORY/var/logs" && \
rm -rf "$APP_DIRECTORY/var/cache" && \
ln -s /home/vagrant/cache/$APP/cache "$APP_DIRECTORY/var/cache"