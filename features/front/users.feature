Feature:
  Scenario: You need to be logged in to ee the dashboard
    When I am on the front
    And I send a "GET" request to "/"
    Then the response status code should be 302

  Scenario: A "super admin" can see the dashboard
    When I am on the front
    And I want an HTTPS request
    And I am authenticated as user with the uuid "1"
    And I send a "GET" request to "/"
    Then the response status code should be 200
