# config valid only for current version of Capistrano
#lock '3.10.1'

# Capistrano configuration
set :format, :pretty
set :log_level, :info
## set :log_level, :debug

# Symfony configuration
set :symfony_directory_structure, 3
set :sensio_distribution_version, 5

set :app_path, "app"
set :web_path, "web"
set :var_path, "var"
set :bin_path, "bin"

set :app_config_path, "app/config"
set :log_path, "var/logs"
set :cache_path, "var/cache"

set :symfony_env,  "prod"
set :symfony_console_path, "bin/console"
set :symfony_console_flags, "--no-debug"

# Composer configuration
set :composer_install_flags, '--no-dev --prefer-dist --optimize-autoloader'

# App configuration
set :application, 'extranet_app_cms'
set :repo_url, 'git@bitbucket.org:biliboo/bert-cms.git'

set :tmp_dir, "/tmp/deploy"
set :assets_install_path, "web"
set :assets_install_flags,  '--symlink'

set :linked_files, ["app/config/parameters.yml", "var/assets.php"]
set :linked_dirs, ["var/logs"]

set :keep_releases, 2

namespace :deploy do
  task :migrate do
    on roles(:db) do
      invoke 'symfony:console', 'isp:core:maintenance:lock'
      invoke 'symfony:console', 'doctrine:migrations:migrate', '--no-interaction'
      invoke 'symfony:console', 'isp:core:maintenance:unlock'
    end
  end
end

namespace :deploy do
  namespace :symlink do
    task :clean do
      on release_roles :all do
        execute :rm, '-rf', current_path.parent.join(current_path.basename)
      end
    end
  end
end

namespace :deploy do
  namespace :fpm do
    task :reload do
      on release_roles :all do
        execute "sudo /etc/init.d/php7.2-fpm reload"
      end
    end
  end
end

namespace :deploy do
  namespace :supervisor do
    task :restart do
      stage = fetch(:stage)
      on release_roles :all do
        execute "sudo /usr/local/bin/supervisorctl restart rabbitmq-cli-consumer_#{stage}.cms.event_broker"
      end
    end
  end
end

before 'deploy:symlink:release', 'deploy:symlink:clean'
after 'deploy:updated', 'deploy:migrate'
after 'deploy:finishing', 'deploy:cleanup'
after 'deploy:finished', 'deploy:supervisor:restart'
#after 'deploy:fpm:reload', 'deploy:supervisor:restart'
