set :stage, :staging

set :ssh_user, 'bert'
set :deploy_to, '/var/www-apps/recette/cms'
server 'test.extranet.lan', user: fetch(:ssh_user), roles: %w{web app db}, port: 22
