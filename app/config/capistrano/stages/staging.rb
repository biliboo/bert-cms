set :stage, :staging

set :ssh_user, 'bert'
set :deploy_to, '/var/www-apps/staging/cms'
server 'vps460404.ovh.net', user: fetch(:ssh_user), roles: %w{web app db}, port: 2225
