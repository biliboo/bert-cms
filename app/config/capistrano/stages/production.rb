set :stage, :prod

set :ssh_user, 'bert'
set :deploy_to, '/var/www-apps/prod/cms'
server 'vps472079.ovh.net', user: fetch(:ssh_user), roles: %w{web app db}, port: 2225
